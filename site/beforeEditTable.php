<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP | MYSQL | REACT EXAMPLE</title> 
    <script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
    <script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
    <!-- Load Babel Compiler -->
    <script crossorigin src="https://cdnjs.cloudflare.com/ajax/libs/babel-standalone/6.26.0/babel.js"></script>
    <script crossorigin src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <link rel="stylesheet" href="/css/bootstrap.css" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </head>
<body>
<div id='root'></div>
  <script type="text/babel">


  class App extends React.Component {
    constructor(props) {
      super(props);
      this.state = { 
        dogs: [],
       };
    }

    componentDidMount() {
      const url = '/api/getData.php'
      axios.get(url).then(response => response.data)
      .then((data) => {
        this.setState({ dogs: data })
        console.log(this.state.dogs)
      });
      this.interval = setInterval(() => {
        this.setState({ time: Date.now() })
      }, 10);
    }

    goToTable(evt){
      evt.preventDefault();
      window.location.href='/editableTable.php'
    }



    render() {
      return (
        <React.Fragment>
        <h1>Doggo Management</h1>
        <h2><button onClick={this.goToTable}>Edit</button></h2>
        <table border='1' width='50%' >
        <thead>
        <tr>
          <th>Name</th>
          <th>Breed</th>
          <th>Sex</th>
        </tr>
        </thead>
        
        {this.state.dogs.map((dog) => (
          <tbody border='1' width='100%'>
          <tr>
            <td>{ dog.name }</td>
            <td>{ dog.breed }</td>
            <td>{ dog.sex }</td>
          </tr>
          </tbody>
        ))} 
          
        </table>
        
        </React.Fragment>
      );
    }
  }
  
  ReactDOM.render(<App />, document.getElementById('root'));
  </script>
</body>
</html>