<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP | MYSQL | REACT EXAMPLE</title> 
    <script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
    <script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
    <!-- Load Babel Compiler -->
    <script crossorigin src="https://cdnjs.cloudflare.com/ajax/libs/babel-standalone/6.26.0/babel.js"></script>
    <script crossorigin src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <link rel="stylesheet" href="/css/bootstrap.css" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head> 
</head>
<body>
<div id='root'></div>
  <script type="text/babel">
    class DoggoForm extends React.Component {
        constructor(props){
            super(props);
            this.state = {
                name: '',
                breed: '',
                sex: '',
            }
        }

        handleFormSubmit( event ) {
            event.preventDefault();

            let formData = new FormData();
            formData.append('name', this.state.name)
            formData.append('breed', this.state.breed)
            formData.append('sex', this.state.sex)

            console.log(axios({
                method: 'post',
                url: '/api/getData.php',
                data: formData,
                config: { headers: {'Content-Type': 'multipart/form-data' }}
            })
            .then(function (response) {
                //handle success
                console.log(response)

            })
            .catch(function (response) {
                //handle error
                console.log(response)
            }));
        }
        render(){
        return (
        <form>
            <label>Name</label>
            <input type="text" name="name" value={this.state.name}
                onChange={e => this.setState({ name: e.target.value })}/>

            <label>Breed</label>
            <input type="text" name="breed" value={this.state.breed}
                onChange={e => this.setState({ breed: e.target.value })}/>

            <label>Sex</label>
            <input type="text" name="sex" value={this.state.sex}
                onChange={e => this.setState({ sex: e.target.value })}/>

            <input type="submit" onClick={e => this.handleFormSubmit(e)} value="Create Doggo"/>
        </form>);
      
      }
    }


  class App extends React.Component {
    constructor(props) {
      super(props);
      this.state = { 
        dogs: [],
        dogid: '',
        name: '',
        breed: '',
        sex: '',
        time: new Date()
       };
    }

    componentDidMount() {
      const url = '/api/getData.php'
      axios.get(url).then(response => response.data)
      .then((data) => {
        this.setState({ dogs: data })
        console.log(this.state.dogs)
      });
      this.interval = setInterval(() => {
        this.setState({ time: Date.now() })
      }, 0);
    }

    componentWillUnmount(){
      clearInterval(this.interval);
      this.setState({ dogs: [] });
    }

    deleteDog(evt){
      evt.preventDefault();
      let form = new FormData();
      form.append('dogid', this.state.dogid);
      console.log(axios({
                method: 'POST',
                url: '/api/deleteUser.php',
                data: form,
                config: { headers: {'Content-Type': 'multipart/form-data' }}
            })
            .then(function (response) {
                //handle success
                console.log(response)

            })
            .catch(function (response) {
                //handle error
                console.log(response)
            }));
            //this.componentWillUnmount();
            //this.componentDidMount();
            this.setState({dogs: []});
            this.componentDidMount();

            this.mapp();
    }



    editDog(evt){
      evt.preventDefault();
      let form = new FormData();
      form.append('dogid', this.state.dogid);
      form.append('name', this.state.name);
      form.append('breed', this.state.breed);
      form.append('sex', this.state.sex);
      console.log(axios({
        method: 'post',
        url: '/api/editUsers.php',
        data: form,
        config: { headers: {'Content-Type': 'multipart/form-data' }}
      })
      .then(function (response) {
        console.log(response)
      })
      .catch(function (response) {
        console.log(response)
      }));
      //this.componentWillUnmount();
      //this.componentDidMount();
      this.setState({dogs: []});
      this.mapp();
    }


    mapGood(){
      
      return(
      this.state.dogs.map((dog) => (
          <tbody border='1' width='100%'>
          <tr>
            <td><form><input type="text" name="name" value={dog.dogid} onChange={e => this.setState({ dogid: e.target.value })}/></form></td>
            <td><form><input type="text" name="name" defaultValue={dog.name} onChange={e => this.setState({ name: e.target.value })}/></form></td>
            <td><form><input type="text" name="breed" defaultValue={dog.breed} onChange={e => this.setState({ breed: e.target.value })}/></form></td>
            <td><form><input type="text" name="sex" defaultValue={dog.sex} onChange={e => this.setState({ sex: e.target.value })}/></form></td>
            <td><form><input type="submit" onClick={evt => this.editDog(evt)} value="Submit Changes"/></form></td>
            <td><button onClick={evt => this.deleteDog(evt)}>Delete</button></td>
          </tr>
          </tbody>
        )));
    }

    mapp(){
      
      return(
      <table border='1' width='50%' >
        <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Breed</th>
          <th>Sex</th>
          <th>Delete</th>
          <th>Edit</th>
        </tr>
        </thead>
        
        {this.mapGood()} 
          
        </table>
      )
    }

    render() {
      return (
        <React.Fragment>
        <h1>Doggo Management</h1>
        {this.mapp()}
        <DoggoForm />
        
        </React.Fragment>
      );
    }
  }
  
  ReactDOM.render(<App />, document.getElementById('root'));
  </script>
</body>
</html>